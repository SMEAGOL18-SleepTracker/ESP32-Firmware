/*
   PulseSensor measurement manager.
   See https://www.pulsesensor.com to get started.

   Copyright World Famous Electronics LLC - see LICENSE
   Contributors:
     Joel Murphy, https://pulsesensor.com
     Yury Gitman, https://pulsesensor.com
     Bradford Needham, @bneedhamia, https://bluepapertech.com

   Licensed under the MIT License, a copy of which
   should have been included with this software.

   This software is not intended for medical use.
*/
#ifndef PULSE_SENSOR_H
#define PULSE_SENSOR_H

#include <Arduino.h>

class PulseSensor {
public:
  PulseSensor(int8_t inputPin, int8_t ledPin = -1, bool fadeLED = false);

  bool begin(void);

  int16_t getLatestSample(void);
  uint8_t getBeatsPerMinute(void);

  void update(void);

private:
  void _updateLEDs(void);
  //  Average DC Estimator
  int16_t _estimateAverage(uint16_t x, int64_t *estimator);
  //  Low Pass FIR Filter
  int16_t _lowPassFilter(int16_t dataIn);

private:
  // Configuration
  const int8_t _inputPin = -1;
  const int8_t _ledPin = -1;
  const bool _fadeLED = false;
  int16_t _fadeLevel = 0;

  int16_t _currentSignal = 0;
  int16_t _prevSignal = 0;
  int16_t _minPeak = 0;
  int16_t _maxPeak = 0;
  bool _positiveEdge = false;

  int16_t _min = 0;
  int16_t _max = 0;

  uint32_t _lastPeakTime = 0;
  uint32_t _lastBeatTime = 0;
  int16_t _IBIs[10];
  uint8_t _IBIsIterator = 0;
  uint8_t _currentBPM = 0;

  int64_t _avgSignalEstimator = 2048 << 15;
  int16_t _dataBuffer[32];
  uint8_t _dataBufferIterator = 0;
};
#endif // PULSE_SENSOR_H
