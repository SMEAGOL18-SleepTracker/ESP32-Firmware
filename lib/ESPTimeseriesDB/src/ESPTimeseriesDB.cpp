#include "ESPTimeseriesDB.h"
#include "DBStream.h"

#undef min
#undef max

#include <algorithm>

#define INVALID_TIMESTAMP 1

#define KEYFILE_OFFSET 32

#define SECS_YR_2000 (946684800UL)   // the time at the start of y2k

ESPTimeseriesDB::ESPTimeseriesDB(const String dataFilename, const String keyFileName)
    : _dataFileName(dataFilename.indexOf(".") == -1 ? dataFilename + String(".tsDB") : dataFilename),
      _keyFileName(keyFileName.indexOf(".") == -1 ? keyFileName + String(".tsDB") : keyFileName),
      _startOfLastTimestamp(INVALID_TIMESTAMP) {
}

ESPTimeseriesDB::~ESPTimeseriesDB() {
  _dataFile.close();
  _keyFile.close();
}

bool ESPTimeseriesDB::begin(void) {
  uint8_t i = TS_MAX_DATA_FILES;
  // search for the newest datafile
  while (!_dataFile && i != 0) {
    if (SPIFFS.exists(_getDataFileName(i))) {
      _dataFile = SPIFFS.open(_getDataFileName(i), "r+");
      if (_dataFile.size() % 5 != 0) {
        _dataFile = SPIFFS.open(_getDataFileName(1), "w+");
      }
      if (!_dataFile) {
        continue;
      }
      // file opened! now search for the last timestamp
      uint32_t it = _dataFile.size() % 5;
      do {
        _dataFile.seek(it, SeekMode::SeekEnd);
        if (_dataFile.peek() == TS_TIME_ID) {
          _startOfLastTimestamp = _dataFile.position();
          break;
        }
      } while (++it <= _dataFile.size());
    }
    i--;
  }

  // no datafile found. create a new file with index 1
  if (!_dataFile) {
    _dataFile = SPIFFS.open(_getDataFileName(1), "w+");
    if (!_dataFile) {
      return false;
    }
  }

  if (!_keyFile) {
    if (SPIFFS.exists(_keyFileName)) {
      _keyFile = SPIFFS.open(_keyFileName, "r+");
    } else {
      _keyFile = SPIFFS.open(_keyFileName, "w+");
    }
    if (!_keyFile) {
      return false;
    }
    if (_keyFile.size() < KEYFILE_OFFSET) {
      _keyFile.seek(0);
      while (_keyFile.size() < KEYFILE_OFFSET) {
        _keyFile.write(0);
      }
    }
  }
  return true;
}

bool ESPTimeseriesDB::push(const uint32_t timestamp, const String &key, float value) {
  if (!_dataFile && !begin()) {
    return false;
  }
  _dataFile.seek(0, SeekEnd);
  // see if we need to add a new timestamp
  if (_getLastTimeStamp() != timestamp) {
    if (_dataFile.size() + 10 > TS_DATA_FILE_SIZE) {
      _switchToNextFile();
      if (!_dataFile) {
        return false;
      }
    }
    _startOfLastTimestamp = _dataFile.position();
    _dataFile.write(TS_TIME_ID);   // indicates a new timestamp
    _dataFile.write((uint8_t *)&timestamp, 4);
  } else if (timestamp > SECS_YR_2000 && _getLastTimeStamp() > timestamp) {
    return false;   // cannot store past data
  }
  uint8_t ID = _getIndex(key);
  if (ID == TS_INVALID_KEY_ID || ID >= TS_END_ID) {
    return false;
  }
  // check if the value is already represent
  uint32_t i = _startOfLastTimestamp + 5;
  while (i < _dataFile.size()) {
    _dataFile.seek(i);
    if ((uint8_t)_dataFile.peek() == ID) {
      break;
    }
    i += 5;
  }
  if (i >= _dataFile.size())
    _dataFile.seek(0, SeekMode::SeekEnd);
  // add the datapoint
  _dataFile.write(ID);
  _dataFile.write((uint8_t *)&value, 4);
  _dataFile.flush();   // write all data now
  return true;
}

std::vector<DBEntry> ESPTimeseriesDB::queryLatesValues(const String &key, uint8_t maxNumValues, uint32_t beginTimestamp) {
  DBStream stream = getStream(beginTimestamp, Raw, false);
  uint32_t pos    = _dataFile.size();
  uint8_t ID      = _getIndex(key);
  DBEntry entry;
  std::vector<DBEntry> entries;
  entries.reserve(maxNumValues);
  while (pos != 0 && entries.size() < maxNumValues) {
    pos -= 5;
    _dataFile.seek(pos);
    if (_dataFile.read() == ID) {
      _dataFile.read((uint8_t *)&entry.value, 4);
      // search for corresponding timestamp
      while (pos != 0 && entries.size() < maxNumValues) {
        pos -= 5;
        _dataFile.seek(pos);
        if (_dataFile.read() == TS_TIME_ID) {
          _dataFile.read((uint8_t *)&entry.time, 4);
          entries.push_back(entry);
        }
      }
    }
  }
  return entries;
}

bool ESPTimeseriesDB::hasData(uint32_t beginTimestamp) {
  return _getLastTimeStamp() >= beginTimestamp;
}

DBStream ESPTimeseriesDB::getStream(uint32_t beginTimestamp, DBStreamEncoding encoding, bool filter) {
  if (!_dataFile && !begin()) {
    return DBStream(this, std::vector<File>(), 0, std::vector<bool>(), encoding);
  }

  uint8_t dataFileIndex = _getDataFileIndex(_dataFile.name());
  if (dataFileIndex == 0 || dataFileIndex > TS_MAX_DATA_FILES) {
    return DBStream(this, std::vector<File>(), 0, std::vector<bool>(), encoding);
  }

  // search for all files that need to be added
  uint32_t startOfFirstFile = 0;

  int fileIndex = 0;
  bool found    = beginTimestamp < SECS_YR_2000;
  if (!found) {
    fileIndex = dataFileIndex + 1;
  }
  while (fileIndex > 0 && !found) {
    fileIndex--;
    File file = SPIFFS.open(_getDataFileName(fileIndex), "r");
    if (!file) {
      continue;
    }
    uint32_t index     = file.size() % 5;
    uint32_t lastIndex = index;
    uint32_t timestamp = 0;
    do {
      file.seek(file.size() - index);
      if (file.read() == TS_TIME_ID) {
        file.read((uint8_t *)&timestamp, 4);
        if (timestamp > SECS_YR_2000) {
          if (timestamp < beginTimestamp) {
            startOfFirstFile = file.size() - lastIndex;
            found            = true;
          } else if (timestamp == beginTimestamp) {
            startOfFirstFile = file.size() - index;
            found            = true;
          }
          if (found && startOfFirstFile == file.size()) {
            fileIndex++;
            startOfFirstFile = 0;
          }
        }
        lastIndex = index;
      }
      index += 5;
    } while (index < file.size() && !found);
    file.close();
  }
  if (fileIndex <= 0) {
    fileIndex = 1;
  }
  if (fileIndex > dataFileIndex) {
    fileIndex == dataFileIndex;
  }

  std::vector<File> files;
  files.reserve(dataFileIndex - fileIndex + 1);
  for (uint8_t i = fileIndex; i <= dataFileIndex; i++) {
    File file = SPIFFS.open(_getDataFileName(fileIndex), "r");
    if (file) {
      files.push_back(file);
    }
  }

  if (filter)
    return DBStream(this, files, startOfFirstFile, _filterIDs, encoding);
  return DBStream(this, files, startOfFirstFile, std::vector<bool>(), encoding);
}

void ESPTimeseriesDB::writeToStream(Stream *stream, uint32_t beginTimestamp, DBStreamEncoding encoding, bool filter) {
  auto dbStream = getStream(beginTimestamp, encoding, filter);
  while (dbStream.available()) {
    int ret = dbStream.read();
    if (ret < 0) {
      return;
    }
    stream->write(ret);
  }
}

void ESPTimeseriesDB::setStreamFilter(std::vector<String> filterKeys) {
  resetStreamFilter();
  _filterIDs.resize(filterKeys.size(), false);
  uint8_t ID;
  for (auto filterKey : filterKeys) {
    ID = _getIndex(filterKey);
    if (ID >= _filterIDs.size())
      _filterIDs.resize(ID + 1, false);
    _filterIDs[ID] = true;   // flip the specific bit
    _keyFile.seek(ID);
    _keyFile.write(1);
  }
}

void ESPTimeseriesDB::resetStreamFilter(void) {
  _keyFile.seek(0);
  for (int i = 0; i < KEYFILE_OFFSET; i++) {
    _keyFile.write(0);
  }
  _filterIDs.clear();
}

void ESPTimeseriesDB::clear(void) {
  uint8_t i = TS_MAX_DATA_FILES;
  _dataFile.close();
  while (i != 0) {
    if (SPIFFS.exists(_getDataFileName(i))) {
      SPIFFS.remove(_getDataFileName(i));
    }
    i--;
  }
  _dataFile             = SPIFFS.open(_getDataFileName(1), "w+");
  _startOfLastTimestamp = INVALID_TIMESTAMP;
}

size_t ESPTimeseriesDB::currentLength(void) {
  return _dataFile.size();
}

bool ESPTimeseriesDB::shiftTimestamps(uint32_t from, uint32_t to, uint32_t dt) {
  if (!_dataFile && !begin()) {
    return false;
  }
  bool found = false;
  uint32_t timestamp;
  for (uint32_t i = 0; i < _dataFile.size(); i += 5) {
    _dataFile.seek(i);
    if ((uint8_t)_dataFile.read() == TS_TIME_ID) {
      _dataFile.read((uint8_t *)&timestamp, 4);
      if (timestamp >= from && timestamp < to) {
        timestamp += dt;
        _dataFile.seek(i + 1);
        _dataFile.write((uint8_t *)&timestamp, 4);
        found = true;
      }
    }
  }
  _dataFile.flush();   // write all data now
  return found;
}

uint8_t ESPTimeseriesDB::_getIndex(const String &fromKey) {
  uint8_t i      = 0;
  uint32_t index = KEYFILE_OFFSET;
  int ch;

  uint8_t len = fromKey.length() > TS_MAX_KEY_LENGTH ? TS_MAX_KEY_LENGTH : fromKey.length();
  if (len == 0) {
    return TS_INVALID_KEY_ID;
  }
  while (index < _keyFile.size()) {
    _keyFile.seek(index);
    ch = _keyFile.read();
    i  = 0;
    while (ch >= 0 && ch != '\0' && i < len && ch == fromKey[i]) {
      ch = _keyFile.read();
      i++;
    }
    // check if the key equals (only happens if we reach the end of the string)
    if (i == len) {
      return (uint8_t)((index - KEYFILE_OFFSET) / (TS_MAX_KEY_LENGTH + 1)) + 1;
    }
    index += TS_MAX_KEY_LENGTH + 1;
  }
  // could not find the key, push it into the list
  return _addKey(fromKey);
}

const String ESPTimeseriesDB::_getKey(const uint8_t ID) {
  // ID starts at 1, each key is TS_MAX_KEY_LENGTH + 1 long
  size_t start = KEYFILE_OFFSET + (ID - 1) * (TS_MAX_KEY_LENGTH + 1);
  if (start > _keyFile.size()) {
    return String();
  }
  _keyFile.seek(start);
  String key = _keyFile.readStringUntil('\0');
  key.trim();
  return key;
}

uint8_t ESPTimeseriesDB::_addKey(const String &key) {
  _keyFile.seek(0, SeekMode::SeekEnd);
  uint8_t len = key.length() > TS_MAX_KEY_LENGTH ? TS_MAX_KEY_LENGTH : key.length();
  _keyFile.write((uint8_t *)key.c_str(), len);
  while (len < TS_MAX_KEY_LENGTH + 1) {
    _keyFile.write(0);
    len++;
  }
  _keyFile.flush();   // write all data now
  return (_keyFile.size() - KEYFILE_OFFSET) / (TS_MAX_KEY_LENGTH + 1);
}

uint32_t ESPTimeseriesDB::_getLastTimeStamp(void) {
  if (_startOfLastTimestamp == INVALID_TIMESTAMP) {
    return 0;
  } else {
    auto prevPos = _dataFile.position();
    _dataFile.seek(_startOfLastTimestamp + 1);
    uint32_t timestamp = 0;
    _dataFile.read((uint8_t *)&timestamp, 4);
    _dataFile.seek(prevPos);
    return timestamp;
  }
}

void ESPTimeseriesDB::_switchToNextFile(void) {
  _dataFile.seek(0, SeekMode::SeekEnd);
  while (_dataFile.position() < TS_DATA_FILE_SIZE) {
    _dataFile.write(TS_END_ID);
  }
  _dataFile.flush();   // write all data now

  String currentFileName = String(_dataFile.name());
  uint8_t index          = _getDataFileIndex(currentFileName);
  _dataFile.close();
  if (index > TS_MAX_DATA_FILES) {
    return;
  } else if (index == TS_MAX_DATA_FILES) {
    SPIFFS.remove(_getDataFileName(1));                 // remove first file
    for (uint8_t i = 1; i < TS_MAX_DATA_FILES; i++) {   // shift all indicies by one
      SPIFFS.rename(_getDataFileName(i + 1), _getDataFileName(i));
    }
    _dataFile = SPIFFS.open(_getDataFileName(index), "w+");   // create new file with last index
  } else {
    _dataFile = SPIFFS.open(_getDataFileName(index + 1), "w+");
  }

  _startOfLastTimestamp = INVALID_TIMESTAMP;
}

String ESPTimeseriesDB::_getDataFileName(uint8_t index) {
  String filename = _dataFileName;
  filename.replace(".", String(index) + ".");
  if (!filename.startsWith("/"))
    filename = String("/") + filename;
  return filename;
}

uint8_t ESPTimeseriesDB::_getDataFileIndex(String filename) {
  return (uint8_t)filename[filename.indexOf(".") - 1] - '0';
}
