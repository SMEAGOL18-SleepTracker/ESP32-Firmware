#ifndef DBStream_H
#define DBStream_H

#include "Stream.h"
#include "FS.h"
#include <vector>

typedef enum {
  Raw,
  Json
} DBStreamEncoding;

class ESPTimeseriesDB;
class DBStream : public Stream {
public:
  DBStream(ESPTimeseriesDB *db, std::vector<File> files, const uint32_t startOfFirstFile, std::vector<bool> filterIDs, DBStreamEncoding encoding = Json);
  ~DBStream();

  size_t write(uint8_t) override;

  int available() override;
  int read() override;
  int peek() override;
  void flush() override;
  void reset();

private:
  bool _updateBuffer(void);
  File *_getCurrentFile(void);
  bool _fieldContainsValues(File *file);

private:
  ESPTimeseriesDB *_db = nullptr;
  const std::vector<bool> _filterIDs;
  std::vector<File> _files;
  const uint32_t _startOfFirstFile = 0;
  const DBStreamEncoding _encoding = Json;

  uint8_t _filesIndex = 0;
  uint32_t _fileSize  = 0;
  uint32_t _filePos   = 0;

  uint8_t *_buffer   = nullptr;
  uint8_t _bufferPos = 0;
  uint8_t _bufferLen = 0;

  uint32_t _totalLen = 0;
};

#endif   // DBStream_H
