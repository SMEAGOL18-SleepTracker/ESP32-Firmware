#ifndef ESPTimeseriesDB_H
#define ESPTimeseriesDB_H

#include <WString.h>
#include <stdint.h>
#include <vector>

#include <SPIFFS.h>

#include "DBStream.h"

/**
   * \class ESPTimeseriesDB
   *
   * \brief An memory efficient DB implementation, where for each timestamp an
   *        arbitrary amount of key-value pairs can be stored
   *
   *  -----------------------------------------------------
   * | X TIME | V1 VAL | V2 VAL | X TIME | V3 VAL | V2 VAL |
   *  -----------------------------------------------------
   * |--------|--------|            ^
   *   5 Byte   5 Byte           new time

   * The DB uses a fixed size of 5 bytes for each entry.
   * A fixed number of 255 (called X in the graphic above) indicates a new timestamp.
   * After the X a 4 Byte timestmap is stored, representing the time for the following
   * key-value pairs.
   *
   * The values are also always 5 Bytes long : 1 Byte for the key and 4 Bytes for the
   * value.
   * In order to ensure, that the key is only one byte long, a simple lookup-table is
   * used to convert the string to a 1 Byte ID. 255 may not be used as an ID, since it
   * is reserved as indicator for a new timestamp.
   * If a key is not stored in the lookup table, it will be added.
   *
   *
   * \author $Author: Benedikt Eliasson$
   * Contact: Benedikt@Rysta.io
   *
   */

#define TS_DEFAULT_KEY_FILENAME "keys.tsDB"
#define TS_MAX_KEY_LENGTH 16

#define TS_DEFAULT_DATA_FILENAME "data.tsDB"
#define TS_DATA_FILE_SIZE 4069
#define TS_MAX_DATA_FILES 4

#define TS_END_ID 255
#define TS_TIME_ID 254
#define TS_INVALID_KEY_ID 0

typedef struct {
  uint32_t time;
  float value;
} DBEntry;

class ESPTimeseriesDB {
  friend class DBStream;

public:
  ESPTimeseriesDB(const String dataFilename = TS_DEFAULT_DATA_FILENAME, const String keyFileName = TS_DEFAULT_KEY_FILENAME);
  ~ESPTimeseriesDB();

  bool begin(void);

  bool push(const uint32_t timestamp, const String &key, float value);
  bool hasData(uint32_t beginTimestamp);
  void clear(void);

  std::vector<DBEntry> queryLatesValues(const String &key, uint8_t maxNumValues, uint32_t beginTimestamp = 0);

  DBStream getStream(uint32_t beginTimestamp = 0, DBStreamEncoding encoding = Json, bool filter = true);
  DBStream getStream(DBStreamEncoding encoding, bool filter = true) { return getStream(0, encoding, filter); };
  void writeToStream(Stream *stream, uint32_t beginTimestamp = 0, DBStreamEncoding encoding = Json, bool filter = true);

  void setStreamFilter(std::vector<String> filterKeys);
  void resetStreamFilter(void);

  bool shiftTimestamps(uint32_t from, uint32_t to, uint32_t dt);
  size_t currentLength(void);

private:
  uint8_t _getIndex(const String &key);
  uint8_t _addKey(const String &key);
  const String _getKey(const uint8_t index);
  uint32_t _getLastTimeStamp(void);
  void _switchToNextFile(void);

  String _getDataFileName(uint8_t index);
  uint8_t _getDataFileIndex(String filename);

private:
  const String _dataFileName;
  const String _keyFileName;
  File _dataFile;
  File _keyFile;
  std::vector<bool> _filterIDs;

  uint32_t _startOfLastTimestamp = 0;
};

#endif   // ESPTimeseriesDB_H
