/*
    Quaternion.h
    Author: Benedikt Eliasson

    C++ library for basic usage of quaternions.
*/

//------------------------------------------------------------------------------
// Includes

#include "Quaternion.h"
#include <math.h>

//------------------------------------------------------------------------------
// Methods
extern float invSqrt(float x);

Quaternion::Quaternion(void) {
  q[0] = 1.0f;
  q[1] = 0.0f;
  q[2] = 0.0f;
  q[3] = 0.0f;
}

Quaternion::Quaternion(const float w, const float x, const float y,
                       const float z) {
  q[0] = w;
  q[1] = x;
  q[2] = y;
  q[3] = z;
}

Quaternion &Quaternion::normalise(void) {
  float recipNorm =
      invSqrt(q[0] * q[0] + q[1] * q[1] + q[2] * q[2] + q[3] * q[3]);
  return (*this) *= recipNorm;
}

Quaternion Quaternion::conj(void) const {
  return Quaternion(q[0], -q[1], -q[2], -q[3]);
}

EulerAnglesStruct Quaternion::getEulerAngles(void) const {
  EulerAnglesStruct eulerAngles;
  eulerAngles.roll = (atan2(2.0f * (q[2] * q[3] - q[0] * q[1]),
                            2.0f * q[0] * q[0] - 1.0f + 2.0f * q[3] * q[3]));
  eulerAngles.pitch = (-atan(
      (2.0f * (q[1] * q[3] + q[0] * q[2])) /
      sqrt(1.0f - pow((2.0f * q[1] * q[3] + 2.0f * q[0] * q[2]), 2.0f))));
  eulerAngles.yaw = (atan2(2.0f * (q[1] * q[2] - q[0] * q[3]),
                           2.0f * q[0] * q[0] - 1.0f + 2.0f * q[1] * q[1]));
  eulerAngles.roll *= 180 / M_PI;
  eulerAngles.pitch *= 180 / M_PI;
  eulerAngles.yaw *= 180 / M_PI;
  return eulerAngles;
}

VectorStruct Quaternion::rotateVector(float x, float y, float z) {
  Quaternion rot = (*this) * Quaternion(0, x, y, z) * conj();
  VectorStruct vector;
  vector.x = rot[1];
  vector.y = rot[2];
  vector.z = rot[3];
  return vector;
}

float Quaternion::operator[](unsigned int index) const {
  if (index >= 4) {
    return 0;
  }
  return q[index];
}

float &Quaternion::operator[](unsigned int index) {
  if (index >= 4) {
    float dummy = 0;
    return dummy;
  }
  return q[index];
}

Quaternion Quaternion::operator+(const Quaternion &rhs) {
  Quaternion ret(q[0] + rhs[0], q[1] + rhs[1], q[2] + rhs[2], q[3] + rhs[3]);
  return ret;
}

Quaternion Quaternion::operator-(const Quaternion &rhs) {
  Quaternion ret(q[0] - rhs[0], q[1] - rhs[1], q[2] - rhs[2], q[3] - rhs[3]);
  return ret;
}

Quaternion &Quaternion::operator+=(const Quaternion &rhs) {
  q[0] += rhs[0];
  q[1] += rhs[1];
  q[2] += rhs[2];
  q[3] += rhs[3];
  return (*this);
}

Quaternion &Quaternion::operator-=(const Quaternion &rhs) {
  q[0] -= rhs[0];
  q[1] -= rhs[1];
  q[2] -= rhs[2];
  q[3] -= rhs[3];
  return (*this);
}

bool Quaternion::operator==(const Quaternion &rhs) const {
  return ((q[0] == rhs[0]) && (q[1] == rhs[1]) && (q[2] == rhs[2]) &&
          (q[3] == rhs[3]));
}

Quaternion Quaternion::operator*(float rhs) {
  Quaternion ret;
  ret[0] = q[0] * rhs;
  ret[1] = q[1] * rhs;
  ret[2] = q[2] * rhs;
  ret[3] = q[3] * rhs;
  return ret;
}

Quaternion &Quaternion::operator*=(float rhs) {
  q[0] *= rhs;
  q[1] *= rhs;
  q[2] *= rhs;
  q[3] *= rhs;
  return (*this);
}

Quaternion operator*(float lhs, const Quaternion &rhs) {
  Quaternion ret;
  ret[0] = rhs[0] * lhs;
  ret[1] = rhs[1] * lhs;
  ret[2] = rhs[2] * lhs;
  ret[3] = rhs[3] * lhs;
  return ret;
}

Quaternion Quaternion::operator*(const Quaternion &rhs) {
  Quaternion ret;
  ret[0] = q[0] * rhs[0] - q[1] * rhs[1] - q[2] * rhs[2] - q[3] * rhs[3];
  ret[1] = q[0] * rhs[1] + q[1] * rhs[0] + q[2] * rhs[3] - q[3] * rhs[2];
  ret[2] = q[0] * rhs[2] - q[1] * rhs[3] + q[2] * rhs[0] + q[3] * rhs[1];
  ret[3] = q[0] * rhs[3] + q[1] * rhs[2] - q[2] * rhs[1] + q[3] * rhs[0];
  return (ret);
}

//---------------------------------------------------------------------------------------------------
// Fast inverse square-root
// See: http://en.wikipedia.org/wiki/Fast_inverse_square_root

float invSqrt(float x) {
  float halfx = 0.5f * x;
  float y = x;
  long i = *(long *)&y;
  i = 0x5f3759df - (i >> 1);
  y = *(float *)&i;
  y = y * (1.5f - (halfx * y * y));
  return y;
}
//------------------------------------------------------------------------------
// End of file
