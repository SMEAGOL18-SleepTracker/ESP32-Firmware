#ifndef Microphone_H
#define Microphone_H

#include <Arduino.h>

#define MIC_MEAN 1870

class Microphone {
public:
  Microphone(int8_t analogIn);
  ~Microphone() = default;

  bool begin(void);
  void update(void);

  uint16_t getLastSample(void);
  uint16_t getMax(void);
  void resetMax(void);

  float getRMS(void);
  void resetRMS(void);

  void setAutoResetMax(bool autoResetMax) { _autoResetMax = autoResetMax; };
  void setAutoResetRMS(bool autoResetRMS) { _autoResetRMS = autoResetRMS; };

private:
  const int8_t _analogIn = -1;
  uint16_t _lastSample = 0;

  uint16_t _max = 0;
  bool _autoResetMax = true;

  uint32_t _RMSSum = 0;
  uint16_t _numSamples = 0;
  bool _autoResetRMS = true;
};

#endif // Microphone_H
