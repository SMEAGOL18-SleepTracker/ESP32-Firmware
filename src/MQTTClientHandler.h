#include <Arduino.h>

#include <AsyncMqttClient.h>

class MQTTClientHandler {
public:
  MQTTClientHandler();
  ~MQTTClientHandler();

public:
  AsyncMqttClient *getClient(void) { return _client; }

private:
  void _registerHandles(void);
  void _onConnected(bool sessionPresent);
  void _onDisconnected(AsyncMqttClientDisconnectReason reason);
  void _onSubscribe(uint16_t packetId, uint8_t qos);
  void _onUnsubscribe(uint16_t packetId);
  void _onMessage(char *topic, char *payload,
                  AsyncMqttClientMessageProperties properties, size_t len,
                  size_t index, size_t total);
  void _onPublish(uint16_t packetId);

  AsyncMqttClient *_client;
};
